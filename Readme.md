A prototype implementations of support for NIF in GATE.
=========================================

This is a prototype implementations of NIF support in GATE.
The current code accepts NIF corpus and exports it in form of GATE documents. You can have a look into some available corpora in this repo: https://github.com/AKSW/n3-collection/

##How-to
```sh
$ git clone https://m1ci@bitbucket.org/m1ci/gate-nif-impl.git
$ cd gate-nif-impl/wikilinks-nif-load/
$ mvn clean compile assembly:single
$ cd target
$ java -jar wikilinks-nif-load-1.0-jar-with-dependencies.jar ../../datasets/wikilinks-small-sample.ttl
```
Check the "docs/" folder. It should contain GATE documents created from the NIF datasets.

We are currently working on providing wider support for NIF in GATE. 
#####Stay tuned!

License
------

Licensed under the [GNU General Public License Version 3 (GNU GPLv3)](http://www.gnu.org/licenses/gpl.html).

Copyright (c) 2014 Milan Dojchinovski (<milan.dojchinovski@fit.cvut.cz>)