/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.ctu.fit.wikilinks.nif.load;

/**
 *
 * @author Milan Dojchinovski <milan.dojchinovski@fit.cvut.cz>
 * @m1ci
 * http://dojchinovski.mk
 */
public class ITS20 {
    public static final String uri = "http://www.w3.org/2005/11/its/rdf#";
    public static final String taIdentRef = uri+"taIdentRef";
    public static final String taClassRef = uri+"taClassRef";
}
