/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.ctu.fit.wikilinks.nif.load;

/**
 *
 * @author Milan Dojchinovski <milan.dojchinovski@fit.cvut.cz>
 * @m1ci
 * http://dojchinovski.mk
 */
public class NIF {
    
    public static final String nifNS = "http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#";
    
    // NIF Core Ontology
    // Classes
    public static final String RFC5147String = nifNS+"RFC5147String";
    public static final String Context = nifNS+"Context";
    public static final String String = nifNS+"String";
    public static final String Phrase = nifNS+"Phrase";
    // Predicates
    public static final String isString = nifNS+"isString";
    public static final String beginIndex = nifNS+"beginIndex";
    public static final String endIndex = nifNS+"endIndex";
    public static final String referenceContext = nifNS+"referenceContext";
    public static final String anchorOf = nifNS+"anchorOf";
        
}
