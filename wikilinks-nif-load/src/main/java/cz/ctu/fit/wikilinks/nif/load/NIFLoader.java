/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.ctu.fit.wikilinks.nif.load;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.vocabulary.RDF;
import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.creole.ResourceInstantiationException;
import gate.util.GateException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Milan Dojchinovski <milan.dojchinovski@fit.cvut.cz>
 * @m1ci
 * http://dojchinovski.mk
 */
public class NIFLoader implements INif {
    
    Corpus corpus = null;
    
    /**
    * The function reads a NIF/RDF file and stores the available information
    * in a GATE friendly format.
    * Steps:
    *  - each found nif:Context concept is considered as a GATE document
    *  - an annotation is created for each detected nif:String concept in a nif:Context
    *    - the nif:beginIndex and nif:endIndex are considered as start and end
    *      offset for the new annotation
    *  - the pointer itsrdf:taIdentRef identifying the string target is stored
    *    as annotation feature
    *  - finally, the GATE documents are exported (in XML) and you can load them
    *    in your GATE desktop version
    * 
    */
    public void load(String nifFileLocation) {
        
        try {
            
            corpus = Factory.newCorpus("my corpus");
                        
            Model model = ModelFactory.createDefaultModel();
            
            Property pBegin      = model.createProperty(NIF.beginIndex);
            Property pEnd        = model.createProperty(NIF.endIndex);
            Property pIsString   = model.createProperty(NIF.isString);
            Property pAnchorOf   = model.createProperty(NIF.anchorOf);
            Property pTaIdentRef     = model.createProperty(ITS20.taIdentRef);
            Property pRefContext = model.createProperty(NIF.referenceContext);
            
            Resource cContext   = model.createResource(NIF.Context);
            Resource cString    = model.createResource(NIF.String);
            Resource cPhrase    = model.createResource(NIF.Phrase);
            
//            InputStream in = FileManager.get().open( "/Users/Milan/Documents/research/repositories/gate-nif-impl/wikilinks-small-sample.ttl");
            InputStream in = FileManager.get().open( nifFileLocation );
            model.read(in, null, "TURTLE");
            
            StmtIterator ctxIter = model.listStatements( new SimpleSelector(null, RDF.type,  cContext));
            
            // Iterate through each nif:Context in the RDF document.
            while (ctxIter.hasNext()) {
                
                Statement stm = ctxIter.next();
                RDFNode subject = stm.getSubject();
//                System.out.println(subject.asResource().getURI());
                
                // Get the nif:Context string.
                Statement isStringStm = subject.asResource().getProperty(pIsString);
                String ctxStr = isStringStm.getObject().asLiteral().getString();
                Document doc = Factory.newDocument(ctxStr);
                doc.setParameterValue("encoding", "UTF-8");
                // Iterate through each nif:String within the nif:Context.
                StmtIterator strIter = model.listStatements( new SimpleSelector(null, pRefContext,  subject.asResource()));                
                while(strIter.hasNext()) {
                    
                    Statement strStm = strIter.next();
                    
                    RDFNode str = strStm.getSubject();
                    
                    // Get underlying string for nif:String.
                    Statement strAnchorOfStm = str.asResource().getProperty(pAnchorOf);
                    String anchorStr = strAnchorOfStm.getObject().asLiteral().getString();
//                    System.out.println(anchorStr);
                    
                    // Get begin index for nif:String.
                    Statement beginIndexStm = str.asResource().getProperty(pBegin);
                    int beginIndex = beginIndexStm.getObject().asLiteral().getInt();
                    
                    // Get end index for nif:String.
                    Statement endIndexStm = str.asResource().getProperty(pEnd);
                    int endIndex = endIndexStm.getObject().asLiteral().getInt();

//                    System.out.println(beginIndex+":"+endIndex);
                    // Get DBpedia itsrdf:taIdentRef for nif:String.
                    Statement taIdentRefStm = str.asResource().getProperty(pTaIdentRef);
                    String taIdentRefURI = taIdentRefStm.getObject().asResource().getURI();
                    
                    AnnotationSet as_default = doc.getAnnotations();
                    
                    // Prepare nif:String annotation.
                    FeatureMap featureMap = null;
                    featureMap = Factory.newFeatureMap();

                    // Add taIdentRef as annotation feature.
                    featureMap.put("itsrdf:taIdentRef", taIdentRefURI);                    
                    
                    // Create the annotation.
                    as_default.add(new Long(beginIndex), new Long(endIndex), "nif:String", featureMap);
                    
//                    System.out.println(taIdentRefURI);
                }                
                // Add the document to the corpus.
                corpus.add(doc);
            }            
        } catch (ResourceInstantiationException ex) {
            Logger.getLogger(NIFLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(NIFLoader.class.getName()).log(Level.INFO, ex.toString());
        }
    }    

    public void initGATE() {
        try {
            File gateHomeFile = new File("/Applications/GATE_Developer_7.1");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.1/plugins            
            File pluginsHome = new File("/Applications/GATE_Developer_7.1/plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.1/user.xml
            Gate.setUserConfigFile(new File("/Applications/GATE_Developer_7.1", "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
        } catch (GateException ex) {
            Logger.getLogger(NIFLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void storeResults() {
        
        FileWriter fw = null;
        
        try {
             File theDir = new File("docs");

            // if the directory does not exist, create it
            if (!theDir.exists()) {

              try {
                  theDir.mkdir();
               } catch(SecurityException se){
                  // no rights
               }
            }
            
            for(int i = 0; i<corpus.size(); i++) {            
                Document doc = corpus.get(i);
                File file = new File("./docs/doc-"+i+".xml");
                fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);        
                bw.write(doc.toXml());
                bw.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(NIFLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println("=====================================================================");
            System.out.println("Processed "+corpus.size()+" nif:Context documents.");
            System.out.println("See the ./docs/ folder for the results exported as UTF-8 encoded GATE documents.");
            System.out.println("You can import new these documents into your GATE desktop version.");
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(NIFLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
