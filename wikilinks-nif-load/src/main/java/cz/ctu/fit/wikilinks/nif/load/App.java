package cz.ctu.fit.wikilinks.nif.load;

public class App {
    
    public static void main( String[] args ) {
        
        String nifFileLocation = null;
        if(args.length == 0) {
            System.out.println( "Enter the location of the dataset" );
        } else {
            nifFileLocation = args[0];
//            String nifFileLocation = "/Users/Milan/Documents/research/repositories/gate-nif-impl/datasets/News-100.ttl";
            INif nifLoader = new NIFLoader();
            nifLoader.initGATE();
            nifLoader.load(nifFileLocation);
            nifLoader.storeResults();
        }
    }
}
