/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.ctu.fit.wikilinks.nif.load;

/**
 *
 * @author Milan Dojchinovski <milan.dojchinovski@fit.cvut.cz>
 * @m1ci
 * http://dojchinovski.mk
 */
public interface INif {
    
    public void initGATE();
    public void load( String nifFileLocation );
    public void storeResults();
}
